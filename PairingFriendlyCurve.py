import sage

try:
    from exceptions import ValueError
except ModuleNotFoundError:
    pass
from sage.functions.log import log
from sage.functions.other import ceil
from sage.arith.functions import lcm
from sage.arith.misc import GCD, gcd
from sage.arith.misc import XGCD, xgcd
from sage.rings.integer import Integer
from sage.rings.integer_ring import Z, ZZ
from sage.rings.finite_rings.finite_field_constructor import FiniteField, GF
from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
from sage.schemes.elliptic_curves.ell_finite_field import EllipticCurve_finite_field
from sage.schemes.elliptic_curves.constructor import EllipticCurve
from sage.schemes.elliptic_curves.cm import hilbert_class_polynomial

def get_curve_parameter_a_j1728(E_tr, E_y, E_beta, E_p, E_Fp):
    # computes 'a' such that y^2 = x^3 + a*x / Fp has order p+1-tr
    # Rubin Silverberg, Choosing the correct elliptic curve in the CM method
    # https://eprint.iacr.org/2007/253
    # Algorithm 3.4
    # U = tr/2, V = y/2, d=1, p = (t^2 + y^2)/4 but in KSS16.py, D=4 and y <-> y/2
    U = E_tr // 2
    V = E_y
    if (U % 2) == 1 and ((U-1) % 4  == (V % 4)):
        return Integer(-1), E_Fp(-1)
    if (U % 2) == 1 and ((U-1) % 4  != (V % 4)):
        # return any 'a' a square but not a fourth power : a^((p-1)/2) == 1, a^((p-1)/4) == -1 mod p
        a = 1
        ap = E_Fp(1)
        while not (ap.is_square() and ap**((E_p-1)//4) == -1):
            a += 1
            ap += 1
        return Integer(-a), -ap
    if (U % 2) == 0:
        if ((V-1) % 4 != (U % 4)):
            V = -V
        a = 1
        ap = E_Fp(1)
        #while ap**((E_p-1)/4) != (E_Fp(U)/E_Fp(V)):
        while ap**((E_p-1)/4) != E_Fp(E_beta):
            a += 1
            ap += 1
        return Integer(-a), -ap


def get_curve_parameter_b_j0(E_tr, E_y, E_beta, E_p, E_Fp):
    # computes 'b' such that y^2 = x^3 + b / Fp has order p+1-tr
    # that is, find b not a square, not a cube
    # How to choose the correct twist, knowing only b, without computing
    # the order of the curve? Without even defining a curve?
    # Rubin Silverberg, Choosing the correct elliptic curve in the CM method
    # https://eprint.iacr.org/2007/253
    # Algorithm 3.5
    # U = tr/2, V = y/2 so that p = U^2 + 3*V^2 (4*p = t^2 + d*y^2)
    UU = E_tr # 2U
    VV = E_y  # 2V
    if (VV % 3) == 0 and ((UU % 3) == 2):
        E_b = Integer(16)
        E_bp = E_Fp(16)
    elif (VV % 3) == 0 and ((UU % 3) == 1):
        # b should be a cube but not a square
        # if p = 3 mod 4, -1 is not a square
        # if p = 1,7 mod 8, 2 is a square
        E_b = Integer(2)
        E_bp = E_Fp(2)
        while E_bp.is_square() or (E_bp**((E_p-1)//3) != E_Fp(1)):
            E_b += 1
            E_bp += 1
        E_b = E_b*16
        E_bp = E_bp*16
    else: # we cannot have 3|UU and p=(UU/2)^2 + 3*(VV/2)^2 prime
        if (VV % 3) == 2:
            #val_ = 2*U/(-3*V-U) mod p = beta
            # val = 2U/(-3*V+U) mod p = beta + 1
            VV = -VV
            val_ = E_beta
            val = E_beta+1
        else:
            #val_ = 2*U/(3*V-U) mod p = -beta-1
            #val = 2*U/(3*V+U) mod p = -beta
            val_ = -E_beta-1
            val = -E_beta
        if (UU % 3) == 2:
            # computes val_=2U/(3V-U) mod p and find b: b^((p-1)/6)=2U/(3V-U) mod p
            E_b = Integer(1)
            E_bp = E_Fp(1)
            while (E_bp**((E_p-1)//6) != val_):
                E_b += 1
                E_bp += 1
            E_b = E_b*16
            E_bp = E_bp*16
        else:
            # computes val=2U/(3V+U) mod p and find b: b^((p-1)/6) = 2U/(3V+U) mod p
            E_b = Integer(1)
            E_bp = E_Fp(1)
            while (E_bp**((E_p-1)//6) != val):
                E_bp += 1
                E_b += 1
            E_b = E_b*16
            E_bp = E_bp*16
    return E_b, E_bp

def get_curve_generator_order_r(E):
    E_ap = E.ap()
    E_bp = E.bp()
    E_c = E.c()
    E_r = E.r()
    E_p = E.p()
    E_Fp = E.Fp()
    E_Fpz, z = E.Fpz()

    Gx = E_Fp(0)
    Gfound = False
    while not Gfound:
        Gx += 1
        Gy2 = Gx**3 + E_ap * Gx + E_bp
        while not Gy2.is_square():
            Gx += 1
            Gy2 = Gx**3 + E_ap * Gx + E_bp
        Gy = Gy2.sqrt()
        E_G = E([Gx,Gy])
        # needs to take into account the cofactor
        E_G = E_c*E_G # so that G has exactly order r
        Gfound = (E_G != E(0) and E_r*E_G == E(0))
    return E_G
    
def get_curve_generator_order_r_j1728(E):
    E_ap = E.ap()
    E_c = E.c()
    E_r = E.r()
    E_p = E.p()
    E_Fp = E.Fp()
    E_Fpz, z = E.Fpz()

    Gx = E_Fp(0)
    Gfound = False
    while not Gfound:
        Gx += 1
        Gy2 = Gx**3 + E_ap * Gx
        while not Gy2.is_square():
            Gx += 1
            Gy2 = Gx**3 + E_ap * Gx
        Gy = Gy2.sqrt()
        E_G = E([Gx,Gy])
        # needs to take into account the cofactor
        E_G = E_c*E_G # so that G has exactly order r
        Gfound = (E_G != E(0) and E_r*E_G == E(0))
    return E_G
    
def get_curve_generator_order_r_j0(E):
    E_bp = E.bp()
    E_c = E.c()
    E_r = E.r()
    E_p = E.p()
    E_Fp = E.Fp()
    E_Fpz, z = E.Fpz()
    
    Gy = E_Fp(0)
    Gfound = False
    while not Gfound:
        Gy += 1
        Gx3 = Gy**2 - E_bp
        #while len((z**3 - Gx3).roots()) == 0 :
        while (Gx3**((E_p-1)//3) != E_Fp(1)):
            Gy += 1
            Gx3 = E_Fp(Gy**2 - E_bp)
        Gx = (z**3 - Gx3).roots()[0][0]
        E_G = E([Gx,Gy])
        # needs to take into account the cofactor
        E_G = E_c*E_G # so that G has exactly order r
        Gfound = (E_G != E(0) and E_r*E_G == E(0))
    return E_G

def print_parameters(E):
    # https://docs.python.org/2/library/string.html#format-string-syntax
    print("u ={: d}".format(E._u))
    print("p ={: d}".format(E._p))
    print("r ={: d}".format(E._r))
    print("t ={: d}".format(E._tr))
    print("y ={: d}".format(E._y))
    print("c ={: d} # cofactor".format(E._c))
    print("u ={: #x}".format(E._u))
    print("p ={: #x}".format(E._p))
    print("r ={: #x}".format(E._r))
    print("t ={: #x}".format(E._tr))
    print("y ={: #x}".format(E._y))
    print("c ={: #x} # cofactor".format(E._c))
    print("log_2 p   ={0:8.2f}, p   {1:5d} bits".format(float(log(E._p,2)), E._p.nbits()))
    print("log_2 p^k ={0:8.2f}, p^k {1:5d} bits".format(float(E._k*log(E._p,2)), (E._p**E._k).nbits()))
    print("log_2 r   ={0:8.2f}, r   {1:5d} bits, {2:2d} machine words of 64 bits".format(float(log(E._r,2)), E._r.nbits(), ceil(log(E._r,2)/64)))

def print_parameters_for_RELIC(E):
    print("p in hexa, on 64-bit words, lowest significant word first.")
    i=0
    for pi in E._p.digits(2**64):
        # fill at 16 hexa digits, + 2 for the 'prefix '0x' makes 18 characters
        print("P{0} {1:0=-#018x}".format(i, pi))
        i+=1
    # p_w = p-wordsize rounded to largest machine word
    # we need the inverse of -p, not of p
    # so either
    #g,u0,v0 = xgcd(E._p,2**64) # g = u0*p + v0*2**64
    #u0 = 2**64 - u0
    # or directly:
    g,u0,v0 = xgcd(-E._p,2**64) # g = u0*p + v0*2**64
    if u0 < 0:
        u0 = 2**64+u0
    print("U0 {:0=-#018x}".format(u0))
    #params for the ep module in RELIC

    print("a    ={:= #x} #({})".format(Integer(E._a), E._a))
    print("b    ={:= #x} #({})".format(Integer(E._b), E._b))
    if hasattr(E, '_beta'):
        print("BETA ={:= #x}".format(E._beta))
    if hasattr(E, '_lamb'):
        print("LAMB ={:= #x}".format(E._lamb))
    
    print("Gx ={:= #x}".format(Integer(E._Gx)))
    print("Gy ={:= #x}".format(Integer(E._Gy)))



def check_order(Fp,p,tr,a,b):
    curve_order = p+1-tr
    twist_order = p+1+tr
    E = EllipticCurve([0,0,0,Fp(a),Fp(b)])
    E0 = E(0)
    ok = True
    twist_ok = False
    for i in range(10):
        P = E.random_element()
        if curve_order*P != E0:
            ok = False
            if twist_order*P == E0:
                twist_ok = True
            break
    return ok, twist_ok

def compute_a_b(D,p,tr,y=None):
    if D < 0:
        D = -D
    p = Integer(p)
    tr = Integer(tr)
    Fp = GF(p, proof=False)
    if D == 1 or D == 4 or D == 3:
        if y == None:
            assert ((4*p-tr**2) % D) == 0
            y = ZZ(sqrt((4*p-tr**2)//D))
        else:
            y = Integer(y)
        assert tr**2+D*y**2 == 4*p
    if D==1 or D == 4:
        # beta s.t. beta^2 = -1 mod p -> beta = tr/y mod p
        # invert y mod p
        #g,u,v = xgcd(y,p) # such that g = u*y + v*p
        #beta = (u*tr) % p
        beta = Fp(tr)/Fp(y)
        a, ap = get_curve_parameter_a_j1728(tr, y, beta, p, Fp)
        b=0
        return [(a,b)]
    if D == 3:
        # beta: s.t. beta^2+beta+1 = 0 mod p: (-1+sqrt(-3))/2
        beta = (Fp(tr)/Fp(y) -Fp(1))/Fp(2)
        b, bp = get_curve_parameter_b_j0(tr, y, beta, p, Fp)
        a=0
        return [(a,b)]
    if ((-D) % 4) == 2 or ((-D) % 4) == 3:
        D = 4*D
    H=hilbert_class_polynomial(-D)
    print("#    H(-D).degree() is {}".format(H.degree()))
    rH=H.change_ring(Fp).roots()
    min_bnbits = p.nbits()+1
    # can we have a=-3?
    ab = []
    a_wanted = -3
    while len(ab) == 0 and a_wanted < 10:
        min_a = None
        min_b = None
        print("#looking for a={}".format(a_wanted))
        for i,j_expj in enumerate(rH):
            # y^2 + xy = x^3 - (36/(j - 1728))*x - 1/(j - 1728)
            # (y+1/2*x)^2 - 1/4*x^2 = x^3  - (36/(j - 1728))*x - 1/(j - 1728)
            # y' = y+1/2*x
            # y'^2 = x^3 + 1/4*x^2  - (36/(j - 1728))*x - 1/(j - 1728)
            # x' = (x-1/12)
            # y'^2 = x^3 - 1/48*j/(j - 1728)*x + 1/864*j/(j - 1728)
            # (a,b) ~ (a/u^4, b/u^6)
            j = Fp(j_expj[0])
            if j == 0:
                print("unexpected j=0, a=0")
                continue
            elif j == 1728:
                print("unexpected j=1728, b=0")
                continue
            #fac_j = Fp(j)/Fp(j - 1728)
            #a = -fac_j/Fp(48)
            #b = fac_j/Fp(864)
            a = -3*j*(j - 1728)
            b = 2*j*(j - 1728)**2
            u4 = a/Fp(a_wanted)
            if not u4.is_square():
                continue
            u2=u4.sqrt()
            if (p % 4) == 3 and not u2.is_square():
                u2 = -u2
            if not u2.is_square():
                continue
            u=u2.sqrt()
            a = a_wanted
            b = b/u**6
            # check that this is the curve and not the twist
            ok, twist_ok = check_order(Fp,p,tr,a,b)
            if not ok:
                continue
            b_ZZ = ZZ(b)
            if abs(b_ZZ-p) < abs(b_ZZ):
                b_ZZ -= p
            bnbits = b_ZZ.nbits()
            if bnbits < min_bnbits:
                min_bnbits = bnbits
                min_a = a
                min_b = b_ZZ
        if min_a != None and min_b != None:
            ab.append((min_a, min_b))
        if a_wanted == -3:
            a_wanted = 1
        else:
            a_wanted += 1
    if len(ab) == 0:
        j, je = rH[0]
        j = ZZ(j)
        if abs(j-p) < abs(j):
            j = j-p
        print("j={}".format(j))
        a = ZZ(-3*j*(j - 1728))
        b = ZZ(2*j*(j - 1728)**2)
        print("a,b = {},{}".format(a,b))
        # simplify
        g = gcd(a,b)
        gf = g.factor()
        u = 1
        for fac,ei in gf:
            if fac > 1:
                while (a % (fac**2)) == 0 and (b%(fac**3)) == 0:
                    a = a // fac**2 # 4
                    b = b // fac**3 # 6
                    u *= fac
        print("a={}, b= {}, simplified by u = {}".format(a,b,u))
        if abs((a % p)) < abs(a):
            a = a % p
            if abs(a-p) < abs(a):
                a = a-p
        if abs((b % p)) < abs(b):
            b = b % p
            if abs(b-p) < abs(b):
                b = b-p
        # check that this is the curve and no the twist
        ok, twist_ok = check_order(Fp,p,tr,a,b)
        if not ok and not twist_ok:
            print("pb with a,b")
            return []
        ns = -2
        nsp= Fp(-2)
        while not ok and twist_ok: # this is the twist
            # find a non-square
            ns += 1
            nsp += 1
            while nsp.is_square():
                ns += 1
                nsp += 1
            a = a*ns**2
            b = b*ns**3
            ok, twist_ok = check_order(Fp,p,tr,a,b)
            print("ns = {}, try with (a,b) = ({},{})".format(ns,a,b))
        ab = [(a,b)]
    return ab

def convert_CocksPinch567(k,T,r,i,tr,y,D,p):
    p = Integer(p)
    T = Integer(T)
    r = Integer(r)
    y = Integer(y)
    tr = Integer(tr)
    if k == 5:
        # Phi_5(T) = T^4+T^3+T^2+T+1
        if i >= 1 and i <= 3:
            t0 = T**i + 1
        elif i == 4: # T^4+1 = -(T^3+T^2+T+1)+1 = -(T^3+T^2+T)
            t0 = -(T**3+T**2+T)
        else:
            raise ValueError("k=5, i s.t. tr=T^i+1 mod r should be 1 <= i <= 4 but got i = {}".format(i))
    elif k == 6:# i=1, or i=5
        if i == 1:
            t0 = T+1
        elif i ==5:
            t0 = -T+2
        else:
            raise ValueError("k=6, i s.t. tr=T^i+1 mod r should be 1 or 5 but got i = {}".format(i))        
    elif k == 7:
        # Phi_7(T) = T^6+T^5+T^4+T^3+T^2+T+1
        if i >= 1 and i <= 5:
            t0 = T**i + 1
        elif i == 6: # T^6+1 = -(T^5+T^4+T^3+T^2+T+1)+1 = -(T^5+T^4+T^3+T^2+T)
            t0 = -(T**5+T**4+T**3+T**2+T)
        else:
            raise ValueError("k=7, i s.t. tr=T^i+1 mod r should be 1 <= i <= 6 but got i = {}".format(i))
    elif k == 8:# i=1,3,5,7
        if i == 1 or i == 3:
            t0 = T**i+1
        elif i ==5:
            t0 = -T+1
        elif i == 7:
            t0 = -T**3 + 1
        else:
            raise ValueError("k=8, i s.t. tr=T^i+1 mod r should be 1,3,5, or 7 but got i = {}".format(i))
    
    assert (tr-t0) % r == 0
    ht = (tr-t0)//r
    y0 = y % r
    if abs(y0-r) < abs(y0):
        y0 = y0-r
    assert (y-y0) % r == 0
    hy = (y-y0)//r
    if k == 6 and D == 3:
        if (T % 3) == 2:
            raise ValueError("Error T = 2 mod 3, should be 1 or 2")
        if  i == 1:
            if (T % 3) == 0:
                expected_y0 = -(T**2-2*T)//3
            elif (T % 3) == 1:
                expected_y0 = (2+T**2)//3
        elif i == 5:
            if (T % 3) == 0:
                expected_y0 = (-3+2*T-T**2)//3
            elif (T % 3) == 1:
                expected_y0 = (T**2-1)//3
        if y0 == expected_y0:
            pass
        elif y0 == -expected_y0: # y = y0 + hy*r, -y = -y0-hy*r
            y = -y
            hy = -hy
            y0 = -y0
            print("changed y,y0,hy -> -y,-y0,-hy")
        else:
            raise ValueError("Error not able to find compatible y0, k={}, y % r = {}, -y mod r = {}, expected y0 = {}, (y-y0)%r = {}, (y+y0)%r = {}".format(k, (y%r), (-y%r), expected_y0, (y-expected_y0)%r, (y+expected_y0)%r))
        
    assert ((t0 + ht*r)**2 + D*(y0+hy*r)**2) % 4 == 0
    assert p == ((t0 + ht*r)**2 + D*(y0+hy*r)**2)//4

    p = Integer(p)
    r = Integer(r)
    print("    {{\'T\':{:#x},".format(Integer(T)))
    print("     \'k\':{},".format(k))
    print("     \'ht\':{:#x},".format(Integer(ht)))
    print("     \'hy\':{:#x},".format(Integer(hy)))
    print("     \'D\':{},".format(abs(D)))
    print("     \'p\':{:#x},".format(Integer(p)))
    print("     \'r\':{:#x},".format(Integer(r)))
    print("     \'i\':{},".format(i))
    print("     \'t0\':{:#x},".format(Integer(t0)))
    print("     \'tr\':{:#x},".format(Integer(tr)))
    print("     \'y0\':{:#x},".format(Integer(y0)))
    print("     \'y\':{:#x},".format(Integer(y)))
    print("     \'pnbits\':{},".format(p.nbits()))
    print("     \'rnbits\':{},".format(r.nbits()))
    pairs_ab = compute_a_b(D,p,tr)
    for a,b in pairs_ab:
        if a != None and b != None:
            print("     \'a\':{:#x},".format(Integer(a)))
            print("     \'b\':{:#x},".format(Integer(b)))
        else:
            print("     #\'a\':,")
            print("     #\'b\':,")
    print("    },")

def print_JL_polys(C,Y):
    l1 = len(C)
    l2 = len(Y)
    if l1 < l2:
        f,g = Y,C
        l1,l2 = l2,l1
    else:
        f,g = C,Y

    str_f = "     \'JL1\':["
    for fi in f[:-1]:
        str_f += "{},".format(fi)
    str_f += "{}],".format(f[-1])
    str_g = "     #\'JL0\':["
    for gi in g[:-1]:
        str_g += "{},".format(gi)
    str_g += "{}],".format(g[-1])

    str_gx = "     \'JL0\':["
    for gi in g[:-1]:
        str_gx += "{:#x},".format(gi)
    str_gx += "{:#x}],".format(g[-1])
    print(str_f)
    print(str_gx)
    print(str_g)
