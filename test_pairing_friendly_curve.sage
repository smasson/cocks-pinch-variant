"""
Sage script

"""
from CocksPinch6 import CocksPinch6
from CocksPinch8 import CocksPinch8
from CocksPinch_k import CocksPinch_k
from MNT6 import MNT6
from BN import BN
from BLS12 import BLS12
from KSS16 import KSS16

from TestPairingFriendlyCurve import *
from TestVectorPairingFriendlyCurve import *

test_CocksPinch5()
test_CocksPinch7()

test_CocksPinch6_i1()
test_CocksPinch6_i5()

test_CocksPinch8_i1()
test_CocksPinch8_i5()

test_MNT6()
test_BN()
test_BLS12()
test_KSS16()
