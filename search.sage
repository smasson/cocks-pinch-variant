import sys
import re
import os
import pprint
from CocksPinchVariant import *

# See https://medium.com/practo-engineering/threading-vs-multiprocessing-in-python-7b57f224eadb
# from threading import Thread
from multiprocessing import Process

# See README.md for documentation on how to use this search program.

args=sys.argv[1:]

parallel=None

search_args=dict()
search_args["lambdar"] = 256
search_args["allowed_cofactor"] = 16*11*7
search_args["allowed_size_cofactor"] = 10
search_args["allowed_automatic_cofactor"] = 4
# 1 means E
# 2 means Et
# 4 means E2
# 8 means E2t
# 15 means all of them
search_args["check_small_subgroup_secure"]=1
search_args["verbose"]=False

search_args["T_choice"]='random'
search_args["hty_choice"]=''

control_args=dict()
control_args["spawn"]="1"
control_args["parallel-mode"]='T'
control_args["ntasks"]=1
control_args["task"]=0

while args:
    parsed_one = False
    for passthrough in [ "verbose" ]:
        if not args:
            break
        if args[0] == "-" + passthrough or args[0] == "--" + passthrough:
            assert len(args) >= 1
            parsed_one = True
            search_args[passthrough] = True
            args=args[1:]
        if not args:
            break
        if args[0] == "-no-" + passthrough or args[0] == "--no-" + passthrough:
            assert len(args) >= 1
            parsed_one = True
            search_args[passthrough] = False
            args=args[1:]
    for passthrough in [ "k", "lambdap", "lambdar", "allowed_automatic_cofactor", "allowed_cofactor", "allowed_size_cofactor", "check_small_subgroup_secure", "max_poly_coeff", "required_cofactor", "restrict_i", "Drange", "l", "seed" ]:
        if not args:
            break
        if args[0] == "-D" or args[0] == "--D":
            assert len(args) >= 2
            parsed_one = True
            # special case for the --D argument (for brevity)
            search_args["Drange"] = [eval(preparse(args[1]))]
            args=args[2:]
        if args[0] == "-" + passthrough or args[0] == "--" + passthrough:
            assert len(args) >= 2
            parsed_one = True
            search_args[passthrough] = eval(preparse(args[1]))
            args=args[2:]
    for passthrough in [ "T_choice", "hty_choice" ]:
        if not args:
            break
        if args[0] == "-" + passthrough or args[0] == "--" + passthrough:
            assert len(args) >= 2
            parsed_one = True
            search_args[passthrough] = args[1]
            args=args[2:]
    # control booleans
    for control in [ "save", "statefile" ]:
        if not args:
            break
        if args[0] == "-" + control or args[0] == "--" + control:
            assert len(args) >= 1
            parsed_one = True
            control_args[control] = True
            args=args[1:]
    # control ints
    for control in [ "spawn", "task", "ntasks" ]:
        if not args:
            break
        if args[0] == "-" + control or args[0] == "--" + control:
            assert len(args) >= 2
            parsed_one = True
            control_args[control] = int(args[1])
            args=args[2:]
    # control strings
    for control in [ "parallel-mode" ]:
        if not args:
            break
        if args[0] == "-" + control or args[0] == "--" + control:
            assert len(args) >= 2
            parsed_one = True
            control_args[control] = args[1]
            args=args[2:]
    if parsed_one:
        continue
    break


if len(args) == 2:
    control_args["task"] = int(args[0])
    control_args["ntasks"] = int(args[1])
elif args:
    print(args)
    assert False

pk = control_args['parallel-mode']
assert pk == 'T' or pk == 'ht' or pk == 'hy'
parallel=(pk, control_args["task"], control_args["ntasks"])

class worker(Process):
    def __init__(self, id, n, control_args=dict(), **search_args):
        Process.__init__(self)
        self.id = id
        self.n = n
        self.control_args = control_args
        defaults = { 'save': False, 'statefile': None}
        for d,v in defaults.items():
            if d not in self.control_args:
                self.control_args[d] = v
        self.search_args = search_args

    def run(self):
        parallel = (self.control_args['parallel-mode'], self.id, self.n)
        search_args= self.search_args
        for mandatory in [ "k", "lambdar", "lambdap", "Drange" ]:
            if mandatory not in search_args:
                raise TypeError("Mandatory argument %s is missing" % mandatory)
        k = search_args["k"]
        lambdap = search_args["lambdap"]
        base = "curves-k%d-p%d-T:%s-hty:%s-%d-%d" % (k, lambdap, search_args["T_choice"], search_args["hty_choice"], self.id, self.n)
        if control_args["save"]:
            search_args["output_file"]="curves-data/%s.sage" % base
        if control_args["statefile"]:
            assert control_args["save"]
            search_args["statefile"]="curves-data/.state.%s" % base
            test=0
            while os.path.isfile(search_args["output_file"]):
                test += 1
                search_args["output_file"]="curves-data/%s.run%d.sage" % (base, test)
        # pprint.pprint(search_args)
        CocksPinchVariant(parallel=parallel, **search_args)

children=[]

if not int(control_args["spawn"]):
    print("# spawn == 0 ??")
    sys.exit(0)

for i in range(int(control_args["spawn"])):
    children.append(worker(parallel[1] + i, parallel[2], control_args=control_args, **search_args))

print("# The search below can be obtained (for child 0) from inside sage as follows:")
print("attach(\"CocksPinchVariant.py\")")
print("CocksPinchVariant(")
for k,v in children[0].search_args.items():
    print("\t{}={},".format(k, pprint.pformat(v)))
print("\tparallel={})".format(pprint.pformat(parallel)))

for c in children:
    c.start()

for c in children:
    c.join()
