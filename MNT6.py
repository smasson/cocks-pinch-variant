# MNT curves with k=6
# This time, j != 0.
import sage

try:
    from exceptions import ValueError
except ModuleNotFoundError:
    pass
from sage.functions.log import log
from sage.functions.other import ceil
from sage.functions.other import sqrt
from sage.arith.misc import XGCD, xgcd
from sage.rings.integer import Integer
from sage.rings.finite_rings.finite_field_constructor import FiniteField
from sage.rings.polynomial.polynomial_ring_constructor import PolynomialRing
from sage.schemes.elliptic_curves.ell_finite_field import EllipticCurve_finite_field
from sage.schemes.elliptic_curves.constructor import EllipticCurve

import PairingFriendlyCurve
from PairingFriendlyCurve import get_curve_generator_order_r

class MNT6(EllipticCurve_finite_field):
    """
    A Miyaji-Nakabayashi-Takano curve of embedding degree 6
    http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.20.8113&rep=rep1&type=pdf
    """
    def __init__(self, u, a, b, D=None, c=None):
        """
        u is the seed such that p=P_MNT6(u), and a,b are the curve coefficients, 
        s.t. y^2 = x^3 + a*x + b has exactly order r=R_MNT6(u)
        (there are two possible choices for b: b, 1/b (a twist) but only one has order r)
        """
        self._k = 6 # embedding degree
        self._a = Integer(a)
        self._b = Integer(b)
        if D != None:
            self._D = Integer(D)
        # polynomials (will be needed later for polynomial selection)
        #P_MNT6 = 4*x^2+1
        #R_MNT6 = 4*x^2 - 2*x + 1
        #C_MNT6 = 1
        #T_MNT6 = 2*x + 1
        #DY2_MNT6 = 12*x^2 - 4*x + 3
        # no beta, lambda
        self.polynomial_p = [1, 0, 4]
        self.polynomial_p_denom = 1
        self.polynomial_r = [1, -2, 4]
        self.polynomial_r_denom = 1
        self.polynomial_c = [1]
        self.polynomial_c_denom = 1
        self.polynomial_tr = [1, 2]
        self.polynomial_tr_denom = 1

        self._u = Integer(u)
        self._p = Integer(4)*(self._u)**2 + Integer(1)
        self._pbits = self._p.nbits()
        self._tr = Integer(2)*self._u + 1
        self._r = Integer(4)*self._u**2 - Integer(2)*self._u + 1
        if c != None and c != 1 and (self._r % Integer(c)) == 0:
            self._c = Integer(c)
            self._r = self._r // self._c
        else:
            self._c = Integer(1)
        if not self._c * self._r == self._p + 1 - self._tr:
            raise ValueError("Error: r*c != p+1-tr\nr={}\nc={}\np+1-tr={}\n".format(self._r,self._c,self._p+1-self._tr))
        #print("computing y")
        self._y = Integer(((4*self._p - self._tr**2)//self._D).sqrt())
        #print("y done")

        try:
            self._Fp = FiniteField(self._p)
        except ValueError as err:
            print("ValueError creating Fp: {}".format(err))
            raise
        except:
            print("Error creating Fp")
            raise
        if not self._r.is_prime():
            raise ValueError("Error r is not prime")

        self._Fpz = PolynomialRing(self._Fp, names=('z',))
        (self._z,) = self._Fpz._first_ngens(1)

        if a != None:
            try:
                a = Integer(a)
            except:
                raise
            self._a = a
            self._ap = self._Fp(a)
        if b != None:
            try:
                b = Integer(b)
            except:
                raise
            self._b = b
            self._bp = self._Fp(b)

        try:
            # this init function of super inherits from class EllipticCurve_generic defined in ell_generic.py
            # __init__ method inherited from ell_generic
            EllipticCurve_finite_field.__init__(self, self._Fp, [0,0,0,self._ap,self._bp])
        except ValueError as err:
            print("ValueError at EllipticCurve_finite_field.__init__: {}".format(err))
            raise
        except:
            print("An error occupred when initialising the elliptic curve")
            raise
        #print("check order")
        #self.order_checked = super(MNT6,self).order()
        #if self.order_checked != (self._p+1-self._tr):
        #    raise ValueError("Wrong curve order: this one is a twist")
        # print("check order (randomized)")
        self.curve_order = self._p + Integer(1) - self._tr
        self.twist_order = self._p + Integer(1) + self._tr
        for i in range(10):
            P = self.random_element()
            if self.curve_order*P != self(0):
                if self.twist_order*P == self(0):
                    raise ValueError("Wrong curve order: this one is a twist: (p+1+tr)*P = 0\ntr={}\nr={}\np+1+tr={}\n".format(self._tr,self.curve_order,self.twist_order))
                else:
                    self.order_checked = super(MNT6,self).order()
                    raise ValueError("Wrong curve order:\np+1-tr        = {}\np+1+tr        = {}\nchecked order = {}\np             = {}".format(self.curve_order,self.twist_order,self.order_checked,self._p))
        # print("ok")

        # computes a generator
        self._G = get_curve_generator_order_r(self)
        self._Gx = self._G[0]
        self._Gy = self._G[1]

    def _repr_(self):
        return "MNT6 p"+str(self._pbits)+" (Miyaji-Nakabayashi-Takano curve k=6) curve with seed "+str(self._u)+"\n"+super(MNT6,self)._repr_()

    def u(self):
        return self._u
    def T(self):
        return self._u
    def p(self):
        return self._p
    def r(self):
        return self._r
    def c(self):
        return self._c
    def tr(self):
        return self._tr
    def y(self):
        return self._y
    def D(self):
        return self._D
    def a(self):
        return self._a # 0
    def ap(self):
        return self._ap # 0
    def b(self):
        return self._b # Integer
    def bp(self):
        return self._bp # in Fp (finite field element)

    def k(self):
        return self._k
    def Fp(self):
        return self._Fp
    def Fpz(self):
        return self._Fpz, self._z
    def G(self):
        return self._G

    def print_parameters(self):
        PairingFriendlyCurve.print_parameters(self)
        
    def print_parameters_for_RELIC(self):
        PairingFriendlyCurve.print_parameters_for_RELIC(self)

